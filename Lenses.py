#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 23 22:32:49 2022

@author: Sima
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize

data = np.loadtxt('lenses.txt', skiprows=1)

dash_s = data[:,3]
dash_sprime = data[:,4]
#print(dash_sprime)

serror = data[:,7]
sprimeerror = data[:,8]
#print(sprimeerror)

fit, cov = np.polyfit(dash_s, dash_sprime, 1, w=1/sprimeerror, cov=True)
a = np.sqrt(cov[0,0])
b = np.sqrt(cov[1,1])
print(cov)
print(fit)
curve = np.poly1d(fit)

plt.grid()
plt.xlabel('1/s in m', fontsize=12)
plt.ylabel("1/s' in m", fontsize=12)
plt.title ("1/s' as a function of 1/s", fontsize=12)
plt.errorbar(dash_s, dash_sprime, xerr=serror, yerr=sprimeerror, fmt = '+')
plt.plot(dash_s, curve(dash_s), color="red")
plt.show()
dash_f = (fit[1], b)
print('Intercept = ', fit[1],'+-', b)
f = 1/fit[1]
print(('Focal length = ', f))

s = data[:,0]
hi_ho = data[:,9]
uncs = data[:,5]
def monoExp(x, m, t):
    return m * np.exp(-t * x)

guess = [5, 0.05]
params, cv = scipy.optimize.curve_fit(monoExp, s, hi_ho, sigma = uncs, p0=guess)
m, t = params

plt.grid()
plt.errorbar(s, hi_ho, xerr=uncs, yerr=0.005, fmt = '+')
plt.plot(s, monoExp(s, *params), color="red")
plt.title("hi/ho versus s", fontsize=12)
plt.xlabel('s in m', fontsize=12)
plt.ylabel('hi/ho in m', fontsize=12)
plt.show()

print('m=', params[0])
#print('t=', params[1])
print('t=', params[1])

